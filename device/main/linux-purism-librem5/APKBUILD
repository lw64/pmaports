# Reference: <https://postmarketos.org/vendorkernel>
# Maintainer:  Clayton Craft <clayton@craftyguy.net>
# Co-Maintainer: Bhushan Shah <bshah@kde.org>
pkgname=linux-purism-librem5
pkgver=6.0.0
pkgrel=0
_purismrel=1
# <kernel ver>.<purism kernel release>
_purismver=${pkgver}pureos$_purismrel
pkgdesc="Purism Librem 5 phone kernel fork"
arch="aarch64"
_carch="arm64"
_flavor="purism-librem5"
url="https://source.puri.sm/Librem5/linux-next"
license="GPL-2.0-only"
options="!strip !check !tracedeps
	pmb:cross-native
	pmb:kconfigcheck-community
	"
makedepends="
	bash
	bison
	devicepkg-dev
	findutils
	flex
	installkernel
	openssl-dev
	perl
	rsync
	xz
	"
subpackages="$pkgname-dev"
install="$pkgname.post-upgrade"

# Source
_repository="linux"
# kconfig generated with: ARCH=arm64 make defconfig KBUILD_DEFCONFIG=librem5_defconfig
_config="config-$_flavor.$arch"


source="
	$pkgname-$_purismver.tar.gz::https://source.puri.sm/Librem5/linux/-/archive/pureos/$_purismver/linux-pureos-$_purismver.tar.gz
	$_config
"
builddir="$srcdir/$_repository-pureos-$_purismver"

prepare() {
	default_prepare
	REPLACE_GCCH=0 \
		. downstreamkernel_prepare

	# NOTE: only for generating a config based on Purism's defconfig, for rebasing...
	# make ARCH="$_carch" CC="${CC:-gcc}" \
	# 	defconfig KBUILD_DEFCONFIG=librem5_defconfig
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-postmarketOS" \
		LOCALVERSION=".$_purismrel"
}

package() {
	downstreamkernel_package "$builddir" "$pkgdir" "$_carch" "$_flavor"

	make modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/boot/dtbs"

}

dev() {
	provides="linux-headers"
	replaces="linux-headers"

	cd $builddir

	# https://github.com/torvalds/linux/blob/master/Documentation/kbuild/headers_install.rst
	make -j1 headers_install \
		ARCH="$_carch" \
		INSTALL_HDR_PATH="$subpkgdir"/usr
}
sha512sums="
19a6bd83a9ea6f0330228e95dce63a2241867459f6377302b60d929d122f610375ce2c9ed8f4ee9d1a3cf16190fca056c0162dec1c1b3c6a077e7e3a3cf11e4e  linux-purism-librem5-6.0.0pureos1.tar.gz
203a6b2dd6207b0d482e99f89c6125e3ccfca70b338696bde9a0a99cd34aa033a0a8f8d21cbd1df6a8865980cb1c56a0f2eec2e7aa7f02313142aa8559d4f6d6  config-purism-librem5.aarch64
"
