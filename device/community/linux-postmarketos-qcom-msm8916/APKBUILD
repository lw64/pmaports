# Maintainer: Minecrell <minecrell@minecrell.net>
# Co-Maintainer: Nikita Travkin <nikita@trvn.ru>
# Kernel config based on: arch/arm64/configs/msm8916_defconfig

_flavor="postmarketos-qcom-msm8916"
pkgname=linux-$_flavor
pkgver=6.0.2
# When making kernel config changes please also submit a PR to
# https://github.com/msm8916-mainline/linux/blob/master/kernel/configs/pmos.config
# since the kernel config is regenerated on upgrades and all changes will be lost!
pkgrel=0
pkgdesc="Mainline kernel fork for Qualcomm MSM8916 devices"
arch="aarch64 armv7"
url="https://github.com/msm8916-mainline/linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps
	pmb:cross-native
	pmb:kconfigcheck-community
"
makedepends="
	bash
	bison
	findutils
	flex
	gmp-dev
	mpc1-dev
	mpfr-dev
	openssl-dev
	perl
	postmarketos-installkernel
"

# Architecture
case "$CARCH" in
	aarch64) _carch="arm64" ;;
	arm*)    _carch="arm" ;;
esac

# Source
_tag=v${pkgver//_/-}-msm8916
source="
	$pkgname-$_tag.tar.gz::$url/archive/$_tag.tar.gz
	config-$_flavor.aarch64
	config-$_flavor.armv7
"
builddir="$srcdir/linux-${_tag#v}"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$CARCH" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION=$((pkgrel + 1 ))
}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir"/boot/dtbs
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release
}

sha512sums="
566fa5e8cc3a994c5e400e442894cc32dc0b37722351e17b96f9a6538889f3d5d586b5809b34a4638cc0bc52b0cda46d5fde73c6e1e67865f8671971e45de9e4  linux-postmarketos-qcom-msm8916-v6.0.2-msm8916.tar.gz
8f5c655d95c748de63543e482a31581e3388aacd80d7da346f20d6eb4393d8755c791e8a76fc17a8d22e88d46a9af0067900bcafa2de53d891832fd23cd6d78b  config-postmarketos-qcom-msm8916.aarch64
351c3df7e55bee4b16c871629475e82e52e3b38bfc0e950c631fd1b4d39ddf377358fc5723ee65afecefcac117573b1c0b5723400554159905006715c0aa3b85  config-postmarketos-qcom-msm8916.armv7
"
